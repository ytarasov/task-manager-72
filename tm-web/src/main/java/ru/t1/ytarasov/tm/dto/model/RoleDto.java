package ru.t1.ytarasov.tm.dto.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.enumirated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "app_roles")
public class RoleDto {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column(name = "role_type")
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @ManyToOne
    private UserDto user;

    public RoleDto() {
    }

    public RoleDto(@NotNull final RoleType roleType) {
        this.roleType = roleType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

}
