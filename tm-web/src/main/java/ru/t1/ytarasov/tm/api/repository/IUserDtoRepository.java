package ru.t1.ytarasov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.dto.model.UserDto;

@Repository
public interface IUserDtoRepository extends JpaRepository<UserDto, String> {

    @Nullable
    UserDto findByLogin(@NotNull final String login);

    Boolean existsByLogin(@NotNull final String login);

}
