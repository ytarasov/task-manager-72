package ru.t1.ytarasov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ytarasov.tm.api.service.IProjectDtoService;
import ru.t1.ytarasov.tm.api.service.ITaskDtoService;
import ru.t1.ytarasov.tm.util.UserUtil;

@Controller
public class TasksController {

    @Autowired
    private ITaskDtoService taskDtoService;

    @Autowired
    private IProjectDtoService projectDtoService;

    @GetMapping("/tasks")
    public ModelAndView index() throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("tasks");
        modelAndView.addObject("tasks", taskDtoService.findByUserId(UserUtil.getUserId()));
        modelAndView.addObject("projectDtoService", projectDtoService);
        return modelAndView;
    }

}
