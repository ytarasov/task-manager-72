package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.dto.model.TaskDto;

import java.util.Collection;
import java.util.List;

public interface ITaskDtoService {

    @Transactional
    void save(@NotNull TaskDto task);

    @Transactional
    void create();

    Collection<TaskDto> findAll();

    TaskDto findById(@NotNull String id);

    long count();

    boolean existsById(@NotNull String id);

    @Transactional
    void delete(@NotNull TaskDto task);

    @Transactional
    void deleteById(@NotNull String id);

    @Transactional
    void deleteAll(@NotNull List<TaskDto> tasks);

    @Transactional
    void clear();

    @Transactional
    TaskDto createWithUserId(@Nullable String userId) throws Exception;

    @Transactional
    void saveWithUserId(@Nullable String userId, @Nullable TaskDto task) throws Exception;

    List<TaskDto> findByUserId(@Nullable String userId) throws Exception;

    TaskDto findByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    @Transactional
    void deleteByUserId(@Nullable String userId) throws Exception;

    @Transactional
    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    Long countByUserId(@Nullable String userId) throws Exception;

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    @Transactional
    void deleteWithUserId(@Nullable String userId, @Nullable TaskDto task) throws Exception;

    @Transactional
    void deleteAllWithUserId(@Nullable String userId, @Nullable List<TaskDto> tasks) throws Exception;
}
