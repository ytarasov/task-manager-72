package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.IUserDtoService;
import ru.t1.ytarasov.tm.dto.model.CustomUser;
import ru.t1.ytarasov.tm.dto.model.RoleDto;
import ru.t1.ytarasov.tm.dto.model.UserDto;
import ru.t1.ytarasov.tm.enumirated.RoleType;
import ru.t1.ytarasov.tm.exception.AbstractException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @NotNull
    @Autowired
    private IUserDtoService userDtoService;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final UserDto user = findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        final List<RoleDto> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        userRoles.forEach(role -> roles.add(role.getRoleType().name()));
        return new CustomUser(User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

    public UserDto findByLogin(String login) {
        try {
            UserDto user = userDtoService.findByLogin(login);
            return user;
        } catch (AbstractException e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostConstruct
    public void initUsers() throws AbstractException {
        createUser("admin", "admin", RoleType.ADMINISTRATOR);
        createUser("test", "test", RoleType.USER);
    }

    public boolean checkUser(@Nullable final String login,
                             @Nullable final String password,
                             @Nullable final RoleType roleType
    ) throws AbstractException {
        return userDtoService.existsByLogin(login);
    }

    public void createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) throws AbstractException {
        if (checkUser(login, password, roleType)) return;
        userDtoService.create(login, password, roleType);
    }
}
