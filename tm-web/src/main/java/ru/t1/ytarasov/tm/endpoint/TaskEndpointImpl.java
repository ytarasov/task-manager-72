package ru.t1.ytarasov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.api.endpoint.TaskEndpoint;
import ru.t1.ytarasov.tm.api.service.ITaskDtoService;
import ru.t1.ytarasov.tm.dto.model.TaskDto;
import ru.t1.ytarasov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.ytarasov.tm.api.endpoint.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @Override
    @WebMethod
    @PutMapping("/create")
    public TaskDto create() throws Exception {
        return taskDtoService.createWithUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(
            @NotNull
            @WebParam(name = "task")
            @RequestBody final
            TaskDto task
    ) throws Exception {
        taskDtoService.saveWithUserId(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<TaskDto> findAll() throws Exception {
        return taskDtoService.findByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final
            String id
    ) throws Exception {
        return taskDtoService.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() throws Exception {
        return taskDtoService.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final
            String id
    ) throws Exception {
        return taskDtoService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "task")
            @RequestBody final
            TaskDto task
    ) throws Exception {
        taskDtoService.deleteWithUserId(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final
            String id
    ) throws Exception {
        taskDtoService.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "tasks")
            @RequestBody final
            List<TaskDto> tasks
    ) throws Exception {
        taskDtoService.deleteAllWithUserId(UserUtil.getUserId(), tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() throws Exception {
        taskDtoService.deleteByUserId(UserUtil.getUserId());
    }

}