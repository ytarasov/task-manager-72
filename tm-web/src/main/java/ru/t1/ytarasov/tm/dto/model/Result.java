package ru.t1.ytarasov.tm.dto.model;

import org.jetbrains.annotations.NotNull;

public class Result {

    @NotNull
    private Boolean success = true;

    @NotNull
    private String message = "";

    public Result() {
    }

    public Result(@NotNull Boolean success) {
        this.success = success;
    }

    public Result(@NotNull final Exception e) {
        this.success = false;
        this.message = e.getMessage();
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
