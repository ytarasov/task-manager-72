<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>EDIT TASK</h1>

<div id="formEdit">
	<form:form action="/task/edit/${task.id}" method="POST" modelAttribute="task">
	    <form:input type="hidden" path="id" />
		<form:select path="projectId">
			<form:option value="${null}" label="-- // --" />
			<form:options items="${projects}" itemLabel="name" itemValue="id" />
		</form:select>
		<p>NAME: </p><form:input type="text" path="name" />
		<p>DESCRIPTION: </p><form:input type="text" path="description" />
		<p>DATE START: </p><form:input type="date" path="dateStart" />
		<p>DATE FINISH: </p><form:input type="date" path="dateFinish" />
		<p>STATUS</p>
		<form:select path="status">
			<form:options items="${statuses}" itemLabel="displayName" />
		</form:select>
		<br>
		<br>
		<button type="submit">SAVE</button>
	</form:form>
</div>

<jsp:include page="../include/_footer.jsp"/>