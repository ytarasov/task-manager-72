package ru.t1.ytarasov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.ytarasov.tm.api.service.ITaskDtoService;
import ru.t1.ytarasov.tm.configuration.*;
import ru.t1.ytarasov.tm.dto.model.TaskDto;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.marker.UnitWebCategory;
import ru.t1.ytarasov.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@Category(UnitWebCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        DataBaseConfiguration.class,
        SecurityWebApplicationInitializer.class,
        ServiceAuthenticationEntryPoint.class,
        WebApplicationConfiguration.class
})
public class TaskServiceTest {

    @NotNull
    private final TaskDto task1 = new TaskDto("TEST TASK", "TEST TASK");

    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private String userId;

    @Before
    public void setUp() throws Exception {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        task1.setUserId(userId);
        taskDtoService.save(task1);
    }

    @After
    public void tearDown() throws Exception {
        taskDtoService.deleteByUserId(userId);
    }

    @Test
    public void findByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.findByUserId(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.findByUserId(""));
        @Nullable final List<TaskDto> tasks = taskDtoService.findByUserId(userId);
        Assert.assertNotNull(tasks);
        Assert.assertFalse(tasks.isEmpty());
    }

    @Test
    public void findsByUserIdAndId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.findByUserIdAndId(null, task1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.findByUserIdAndId("", task1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.findByUserIdAndId(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.findByUserIdAndId(userId, ""));
        @Nullable final TaskDto task = taskDtoService.findByUserIdAndId(userId, task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task1.getName(), task.getName());
    }

    @Test
    public void countByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.countByUserId(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.countByUserId(""));
        final int expectedSize = taskDtoService.findByUserId(userId).size();
        final int foundSize = taskDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void existsByUserIdAndId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.existsByUserIdAndId(null, task1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.existsByUserIdAndId("", task1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.existsByUserIdAndId(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.existsByUserIdAndId(userId, ""));
        Assert.assertTrue(taskDtoService.existsByUserIdAndId(userId, task1.getId()));
    }

    @Test
    public void deleteByUserIdAndId() throws Exception {
        @NotNull final TaskDto taskToDelete = new TaskDto("TEST DELETE", "TEST DELETE");
        taskToDelete.setUserId(userId);
        taskDtoService.save(taskToDelete);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.deleteByUserIdAndId(null, taskToDelete.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.deleteByUserIdAndId("", taskToDelete.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.deleteByUserIdAndId(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.deleteByUserIdAndId(userId, ""));
        final int expectedSize = taskDtoService.countByUserId(userId).intValue() - 1;
        taskDtoService.deleteByUserIdAndId(userId, taskToDelete.getId());
        final int foundSize = taskDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void saveWithUserId() throws Exception {
        @NotNull final TaskDto taskToSave = new TaskDto("TEST SAVE", "TEST SAVE");
        final int expectedSize = taskDtoService.countByUserId(userId).intValue() + 1;
        taskDtoService.saveWithUserId(userId,taskToSave);
        final int foundSize = taskDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

}
