package ru.t1.ytarasov.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.ytarasov.tm.configuration.*;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.marker.UnitWebCategory;
import ru.t1.ytarasov.tm.util.UserUtil;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitWebCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        DataBaseConfiguration.class,
        SecurityWebApplicationInitializer.class,
        ServiceAuthenticationEntryPoint.class,
        WebApplicationConfiguration.class
})
public class ProjectEndpointTest {

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private final ProjectDto project1 = new ProjectDto("TEST PROJECT 1", "TEST PROJECT 1");

    @NotNull
    private final ProjectDto project2 = new ProjectDto("TEST PROJECT 2", "TEST PROJECT 2");

    @NotNull
    private final ProjectDto project3 = new ProjectDto("TEST PROJECT 2", "TEST PROJECT 3");

    @NotNull
    private final ProjectDto project4 = new ProjectDto("TEST PROJECT 4", "TEST PROJECT 4");

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @Nullable
    private String userId;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        save(project1);
    }

    @After
    public void tearDown() throws Exception {
        @NotNull final String url = BASE_URL + "clear";
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private void save(@NotNull final ProjectDto project) throws Exception {
        @NotNull final String url = BASE_URL + "save";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Nullable
    private ProjectDto findById(@NotNull final String id) throws Exception {
        @NotNull final String url = BASE_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, ProjectDto.class);
    }

    @NotNull
    private List<ProjectDto> findAll() throws Exception {
        @NotNull final String url = BASE_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return Arrays.asList(mapper.readValue(json, ProjectDto[].class));
    }

    private int count() throws Exception {
        @NotNull final String url = BASE_URL + "count";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Long.class).intValue();
    }

    @Test
    public void findAllTest() throws Exception {
        @Nullable final List<ProjectDto> projects = findAll();
        Assert.assertNotNull(projects);
        Assert.assertFalse(projects.isEmpty());
    }

    @Test
    public void findByIdTest() throws Exception {
        @Nullable final ProjectDto project = findById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), project1.getName());
    }

    @Test
    public void countTest() throws Exception {
        final int foundValue = count();
        final int expectedValue = findAll().size();
        Assert.assertEquals(expectedValue, foundValue);
    }

    @Test
    public void existsById() throws Exception {
        @NotNull final String url = BASE_URL + "existsById/" + project1.getId();
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        Assert.assertTrue(mapper.readValue(json, Boolean.class));
    }

    @Test
    public void deleteByIdTest() throws Exception {
        project2.setUserId(userId);
        save(project2);
        @NotNull final String url = BASE_URL + "deleteById/" + project1.getId();
        final int expectedSize = count() - 1;
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        final int foundSize = count();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void deleteTest() throws Exception {
        project3.setUserId(userId);
        save(project3);
        final int expectedSize = count() - 1;
        @NotNull final String url = BASE_URL + "delete";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(project3);
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        final int foundSize = count();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void createTest() throws Exception {
        @NotNull final String url = BASE_URL + "create";
        final int expectedSize = count() + 1;
        mockMvc.perform(MockMvcRequestBuilders.put(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        final int foundSize = count();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void saveTest() throws Exception {
        final int expectedSize = count() + 1;
        save(project4);
        final int foundSize = count();
        Assert.assertEquals(expectedSize, foundSize);
        Assert.assertNotNull(findById(project4.getId()));
    }

}
