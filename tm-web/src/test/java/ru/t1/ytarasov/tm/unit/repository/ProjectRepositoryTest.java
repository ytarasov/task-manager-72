package ru.t1.ytarasov.tm.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.api.repository.IProjectDtoRepository;
import ru.t1.ytarasov.tm.configuration.*;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.marker.UnitWebCategory;

import java.util.UUID;

@Transactional
@WebAppConfiguration
@Category(UnitWebCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        DataBaseConfiguration.class,
        SecurityWebApplicationInitializer.class,
        ServiceAuthenticationEntryPoint.class,
        WebApplicationConfiguration.class
})
public class ProjectRepositoryTest {

    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();

    @NotNull
    @Autowired
    private IProjectDtoRepository projectDtoRepository;

    private ProjectDto project;

    @Before
    public void setUp() {
        project = new ProjectDto("TEST REPOSITORY PROJECT", "TEST REPOSITORY PROJECT");
        project.setUserId(USER_ID);
        projectDtoRepository.save(project);
    }

    @After
    public void tearDown() {
        projectDtoRepository.deleteByUserId(USER_ID);
    }

    @Test
    public void findByUserId() {
        Assert.assertNotNull(projectDtoRepository.findByUserId(USER_ID));
        Assert.assertFalse(projectDtoRepository.findByUserId(USER_ID).isEmpty());
    }

    @Test
    public void findByUserIdAndId() {
        Assert.assertNotNull(projectDtoRepository.findByUserIdAndId(USER_ID, project.getId()));
    }

    @Test
    public void countByUserId() {
        final int expectedSize = projectDtoRepository.findByUserId(USER_ID).size();
        final int foundSize = projectDtoRepository.countByUserId(USER_ID).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void existsByUserIdAndId() {
        Assert.assertTrue(projectDtoRepository.existsByUserIdAndId(USER_ID, project.getId()));
    }

    @Test
    public void deleteProject() {
        @NotNull final ProjectDto projectToDelete = new ProjectDto("TEST DELETE", "TEST DELETE");
        projectToDelete.setUserId(USER_ID);
        projectDtoRepository.save(projectToDelete);
        final int expectedSize = projectDtoRepository.countByUserId(USER_ID).intValue() - 1;
        projectDtoRepository.deleteByUserIdAndId(USER_ID, projectToDelete.getId());
        final int foundSize = projectDtoRepository.countByUserId(USER_ID).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void deleteByUserId() {
        @NotNull final ProjectDto projectToDelete = new ProjectDto("TEST DELETE BY USER ID", "TEST DELETE USER ID");
        @NotNull final String newUserId = "TST_USER_ID_2";
        projectToDelete.setUserId(newUserId);
        projectDtoRepository.deleteByUserId(newUserId);
        final int foundSize = projectDtoRepository.countByUserId(newUserId).intValue();
        Assert.assertEquals(0, foundSize);
    }

}
