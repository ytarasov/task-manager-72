--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

-- Started on 2022-04-12 14:12:54

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE tm;
--
-- TOC entry 3032 (class 1262 OID 16394)
-- Name: tm; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE tm WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'C';


ALTER DATABASE tm OWNER TO postgres;

\connect tm

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3033 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 204 (class 1259 OID 17135)
-- Name: app_project; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_project (
    id character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone,
    description character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    status character varying(255) NOT NULL,
    user_id character varying(255)
);


ALTER TABLE public.app_project OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 17127)
-- Name: app_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_session (
    id character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    role character varying(255),
    user_id character varying(255)
);


ALTER TABLE public.app_session OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 17143)
-- Name: app_task; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_task (
    id character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone,
    description character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    status character varying(255) NOT NULL,
    user_id character varying(255),
    project_id character varying(255)
);


ALTER TABLE public.app_task OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 17115)
-- Name: app_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_user (
    id character varying(255) NOT NULL,
    login character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password_hash character varying(255) NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    middle_name character varying,
    role character varying(255) NOT NULL,
    locked boolean NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL
);


ALTER TABLE public.app_user OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 17109)
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 17104)
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO postgres;

--
-- TOC entry 3025 (class 0 OID 17135)
-- Dependencies: 204
-- Data for Name: app_project; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.app_project VALUES ('56217861-fbf5-4a9b-82ea-1cece99b9979', '2022-04-12 12:22:59.463', '2022-04-12 12:22:59.463', 'test project 1', 'test project 1', 'NOT_STARTED', '6cfc4cfa-440a-4da2-b311-2596aee00986');


--
-- TOC entry 3024 (class 0 OID 17127)
-- Dependencies: 203
-- Data for Name: app_session; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3026 (class 0 OID 17143)
-- Dependencies: 205
-- Data for Name: app_task; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.app_task VALUES ('3dde80a1-46a9-42aa-ba41-1efb7b1a5ef7', '2022-04-12 12:23:37.578', '2022-04-12 12:24:08.417', 'test task 1', 'test task 1', 'NOT_STARTED', '6cfc4cfa-440a-4da2-b311-2596aee00986', '56217861-fbf5-4a9b-82ea-1cece99b9979');


--
-- TOC entry 3023 (class 0 OID 17115)
-- Dependencies: 202
-- Data for Name: app_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.app_user VALUES ('c1e54ae0-f830-47b6-a7a2-6dc059a673d2', 'admin', 'admin@adm.ru', '99faf4eb7b3729a9c8bd5c22512c7a1f', NULL, NULL, NULL, 'ADMIN', false, '2022-04-12 10:35:49.294', '2022-04-12 10:35:49.294');
INSERT INTO public.app_user VALUES ('6cfc4cfa-440a-4da2-b311-2596aee00986', 'test', 'test@tst.ru', 'a850ffd9185df82e6431bb4cc18356ad', NULL, NULL, NULL, 'USUAL', false, '2022-04-12 10:35:49.35', '2022-04-12 10:35:49.35');


--
-- TOC entry 3022 (class 0 OID 17109)
-- Dependencies: 201
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.databasechangelog VALUES ('create_table_user', 'Yuriy Tarasov', 'changelog/changelog-master.xml', '2022-04-12 10:34:51.778871', 1, 'EXECUTED', '8:ea1a817e0a6cd357b3d9a84847d1e4aa', 'createTable tableName=app_user', '', NULL, '3.8.0', NULL, NULL, '9748891741');
INSERT INTO public.databasechangelog VALUES ('app_session_create', 'Yuriy Tarasov', 'changelog/changelog-master.xml', '2022-04-12 10:34:51.797819', 2, 'EXECUTED', '8:f5aeadfd23a5de6152ffcf95914c352a', 'createTable tableName=app_session', '', NULL, '3.8.0', NULL, NULL, '9748891741');
INSERT INTO public.databasechangelog VALUES ('create_table_project', 'Yuriy Tarasov', 'changelog/changelog-master.xml', '2022-04-12 10:34:51.810701', 3, 'EXECUTED', '8:3090af95c619dbbef08ccc62d4a1b625', 'createTable tableName=app_project', '', NULL, '3.8.0', NULL, NULL, '9748891741');
INSERT INTO public.databasechangelog VALUES ('create_table_task', 'Yuriy Tarasov', 'changelog/changelog-master.xml', '2022-04-12 10:34:51.823545', 4, 'EXECUTED', '8:f51714a70645c6b0d1fb169b215258b0', 'createTable tableName=app_task', '', NULL, '3.8.0', NULL, NULL, '9748891741');
INSERT INTO public.databasechangelog VALUES ('create_session_user_key', 'Yuriy Tarasov', 'changelog/changelog-master.xml', '2022-04-12 10:34:51.845653', 5, 'EXECUTED', '8:4117f9fe95a634ecb53ecad06a5a3bdc', 'addForeignKeyConstraint baseTableName=app_session, constraintName=app_session_user_id_fkey, referencedTableName=app_user', '', NULL, '3.8.0', NULL, NULL, '9748891741');
INSERT INTO public.databasechangelog VALUES ('create_project_user_key', 'Yuriy Tarasov', 'changelog/changelog-master.xml', '2022-04-12 10:34:51.851748', 6, 'EXECUTED', '8:2a8ada0953bbdccda7fd3be5624a56d9', 'addForeignKeyConstraint baseTableName=app_project, constraintName=app_project_user_id_fkey, referencedTableName=app_user', '', NULL, '3.8.0', NULL, NULL, '9748891741');
INSERT INTO public.databasechangelog VALUES ('create_task_user_key', 'YuriyTarasov', 'changelog/changelog-master.xml', '2022-04-12 10:34:51.85767', 7, 'EXECUTED', '8:f0f086f028af9a25854fbdcfd653b2a0', 'addForeignKeyConstraint baseTableName=app_task, constraintName=app_task_user_id_fkey, referencedTableName=app_user', '', NULL, '3.8.0', NULL, NULL, '9748891741');
INSERT INTO public.databasechangelog VALUES ('create_task_project_key', 'Yuriy Tarasov', 'changelog/changelog-master.xml', '2022-04-12 10:34:51.863343', 8, 'EXECUTED', '8:7fe2cdd83b23d5909e7543bfec4591a5', 'addForeignKeyConstraint baseTableName=app_task, constraintName=app_task_project_id_fkey, referencedTableName=app_project', '', NULL, '3.8.0', NULL, NULL, '9748891741');


--
-- TOC entry 3021 (class 0 OID 17104)
-- Dependencies: 200
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.databasechangeloglock VALUES (1, false, NULL, NULL);


--
-- TOC entry 2884 (class 2606 OID 17142)
-- Name: app_project app_project_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_project
    ADD CONSTRAINT app_project_pkey PRIMARY KEY (id);


--
-- TOC entry 2882 (class 2606 OID 17134)
-- Name: app_session app_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_session
    ADD CONSTRAINT app_session_pkey PRIMARY KEY (id);


--
-- TOC entry 2886 (class 2606 OID 17150)
-- Name: app_task app_task_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_task
    ADD CONSTRAINT app_task_pkey PRIMARY KEY (id);


--
-- TOC entry 2876 (class 2606 OID 17126)
-- Name: app_user app_user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user
    ADD CONSTRAINT app_user_email_key UNIQUE (email);


--
-- TOC entry 2878 (class 2606 OID 17124)
-- Name: app_user app_user_login_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user
    ADD CONSTRAINT app_user_login_key UNIQUE (login);


--
-- TOC entry 2880 (class 2606 OID 17122)
-- Name: app_user app_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user
    ADD CONSTRAINT app_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2874 (class 2606 OID 17108)
-- Name: databasechangeloglock databasechangeloglock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT databasechangeloglock_pkey PRIMARY KEY (id);


--
-- TOC entry 2888 (class 2606 OID 17156)
-- Name: app_project app_project_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_project
    ADD CONSTRAINT app_project_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.app_user(id);


--
-- TOC entry 2887 (class 2606 OID 17151)
-- Name: app_session app_session_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_session
    ADD CONSTRAINT app_session_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.app_user(id);


--
-- TOC entry 2890 (class 2606 OID 17166)
-- Name: app_task app_task_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_task
    ADD CONSTRAINT app_task_project_id_fkey FOREIGN KEY (project_id) REFERENCES public.app_project(id);


--
-- TOC entry 2889 (class 2606 OID 17161)
-- Name: app_task app_task_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_task
    ADD CONSTRAINT app_task_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.app_user(id);


-- Completed on 2022-04-12 14:12:54

--
-- PostgreSQL database dump complete
--

