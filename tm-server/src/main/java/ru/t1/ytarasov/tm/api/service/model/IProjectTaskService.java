package ru.t1.ytarasov.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.model.Project;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable final String userId, @Nullable final String taskId, @Nullable final String projectId) throws Exception;

    void removeProject(@Nullable final String userId, @Nullable final Project project) throws Exception;

    void removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws Exception;

    void unbindTaskFromProject(@Nullable final String userId, @Nullable final String taskId, @Nullable final String projectId) throws Exception;

}
