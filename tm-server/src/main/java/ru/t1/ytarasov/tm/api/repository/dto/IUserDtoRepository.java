package ru.t1.ytarasov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.dto.model.UserDTO;

@Repository
public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull final String login);

    @Nullable
    UserDTO findByEmail(@NotNull final String email);

    @Modifying
    @Transactional
    void deleteByLogin(@NotNull final String login);

    Boolean existsByLogin(@NotNull final String login);

    Boolean existsByEmail(@NotNull final String email);

}
